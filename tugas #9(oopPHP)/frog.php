<?php

require_once('Animal.php');

class frog extends Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "No";

    public function Jump()
    {
        echo "Jump : hop hop <br> <br>";
    }
}

?>