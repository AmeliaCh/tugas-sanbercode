<?php
require_once('Animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep =new Animal("Shaun");

echo "Name : ". $sheep->name . "<br>";
echo "Legs : ". $sheep->legs . "<br>";
echo "Cold_blooded : ". $sheep->cold_blooded . "<br> <br>";

$kodok = new frog("Buduk");
echo "Name : ". $kodok->name . "<br>";
echo "Legs : ". $kodok->legs . "<br>";
echo "Cold_blooded : ". $kodok->cold_blooded . "<br>";
$kodok->Jump();

$kera = new ape("Kera sakti");
echo "Name : ". $kera->name . "<br>";
echo "Legs : ". $kera->legs . "<br>";
echo "Cold_blooded : ". $kera->cold_blooded . "<br>";
$kera->yell();

?>