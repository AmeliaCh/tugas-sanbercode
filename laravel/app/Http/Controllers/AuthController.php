<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('form/daftar');
    }
    public function welcome(Request $request)
    {
      $firstname = $request['namadepan'];
        $lastname = $request['namabelakang'];

        return view('home', compact('firstname', 'lastname'));
    }
}
