@extends('layout.master')

@section('judul')
 Detail Para Pemain Film
@endsection

@section('content')

<h2 class="text-info">{{$cast->nama}}</h2>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Back</a>

@endsection