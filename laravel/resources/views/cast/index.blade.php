@extends('layout.master')

@section('judul')
 List Para Pemain Film
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary btn-sm mb-4">Tambah Cast</a>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="delete" class='btn btn-danger btn-sm'>
                </form>
            </td>
          </tr>
      @empty
          <tr>
            <td>Tidak ada data yang tersedia</td>
          </tr>
      @endforelse
    </tbody>
  </table>


@endsection