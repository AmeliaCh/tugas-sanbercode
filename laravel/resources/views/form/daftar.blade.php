<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <h1>Buat Account Baru</h1>
        <h3>Sign Up</h3>
        <form action="/welcome" method="post">
            @csrf
            <label> Nama Depan</label> <br>
            <input type="text" name="namadepan"> <br> <br>
            <label>Nama Belakang</label> <br>
            <input type="text" name="namabelakang" > <br> <br>
            <label>Gender:</label> <br> <br>
            <input type="radio" value="Male" name="gender"> Male <br>
            <input type="radio" value="Female" name="gender"> Female<br>
            <input type="radio" value="Other" name="gender"> Other<br> <br>
            <label>Nationality:</label> <br> <br>
            <select name="Negara">
                <option value="Indonesian">Indonesian</option>
                <option value="Japan">Japan</option>
                <option value="Singapore">Singapore</option>
            </select> <br> <br>
            <label>Language Spoken:</label> <br> <br>
            <input type="checkbox" value="B.indo" name="bahasa">Bahasa Indonesia <br>
            <input type="checkbox" value="B.Ing" name="bahasa">English <br>
            <input type="checkbox" value="B.Japan" name="bahasa">Japan <br>
            <input type="checkbox" value="Other" name="bahasa">Other <br> <br>
            <label>Bio:</label> <br> <br>
            <textarea name="Biodata" id="" cols="30" rows="10"></textarea> <br> <br>
            
            <input type="submit" value="welcome">
        </form>
    </body>
</html>