<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@form');
Route::post('/welcome ', 'AuthController@welcome');


Route::get('/table', 'table@task');
Route::get('/data-table',function(){
    return view('halaman/datatables');
});

//crud cast

//create data cast

//masuk ke form cast
Route::get('cast/create', 'CastController@create');

//untuk kirim inputan ke table cast
Route::post('cast', 'CastController@store');

//read data cast

//tampil semua data cast
Route::get('/cast', 'CastController@index');
//detail cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//update data cast
// masuk ke form cast berdasarkan id
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');
//delete data cast
//delete data berdasrkan id
Route::delete('/cast/{cast_id}', 'CastController@destroy');